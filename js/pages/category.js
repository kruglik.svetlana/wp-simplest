(function ($) {
    let cardsContainer = $('.grid-products-block'),
        cards = cardsContainer.find('.card');

    cards.click(function () {
        let productId = $(this).attr('data-product-id'),
            targetPopup = cardsContainer.find('.product-popup.-' + productId);

        targetPopup.show();
    });

})(jQuery);
