window.addEventListener("load", function () {
    let menuButtonOpen = document.querySelector('#menu_toggle_open'),
        menuButtonClose = document.querySelector('#menu_toggle_close'),
        headerMenu = document.querySelector('#header_menu');

    menuButtonOpen.addEventListener('click', function () {
        headerMenu.classList.add('-active');
    });

    menuButtonClose.addEventListener('click', function () {
        headerMenu.classList.remove('-active');
    });

}, false);
