(function ($) {
    let closeButton = $('.popup-close');

    closeButton.click(function () {
        let container = this.closest('.popup-container');

        $(container).hide();
    })
})(jQuery);