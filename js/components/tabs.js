window.addEventListener("load", function () {
    let tabsButtons = document.querySelectorAll('.tabs-section .tab-button');

    tabsButtons.forEach(function(item){
        item.addEventListener('click', function () {
            this.classList.toggle('-active');
            this.nextElementSibling.classList.toggle('-active');
        });
    });
}, false);