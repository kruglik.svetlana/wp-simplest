<?php
if ( ! isset( $content_width ) ) {
	$content_width = 768;
}

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'automatic-feed-links' );
}

//if ( function_exists( 'register_nav_menu' ) ) {
	//register_nav_menu( 'menu', 'Menu' );
//}

if ( function_exists( 'register_sidebar' ) ) {
	register_sidebar( array(
		'name'          => __( 'Widgets', 'simplest' ),
		'id'            => 'widgets',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div><!-- widget -->',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>'
	) );
}


// Menu

add_action( 'after_setup_theme', 'theme_register_nav_menu' );

function theme_register_nav_menu() {
	register_nav_menu( 'header', 'Меню в шапке' );
	register_nav_menu( 'footer', 'Меню в подвале' );
}

// Creating custom Taxonomies

add_action( 'init', 'simple_register_post_types' );

function simple_register_post_types() {
	register_post_type( 'cutlery', [
		'labels'        => [
			'name'          => __( 'Приборы' ), // основное название для типа записи
			'singular_name' => __( 'Cutlery' ), // название для одной записи этого типа
			'add_new'       => __( 'Добавить новый' ), // для добавления новой записи
			'menu_name'     => __( 'Приборы' ), // название меню
		],
		'public'        => true,
		'menu_position' => 4,
		'menu_icon'     => 'dashicons-food',
		'supports'      => [ 'title', 'editor', 'thumbnail', 'post-formats', 'description' ],
		'taxonomies'    => [ 'category', 'tags' ],
		'has_archive'   => true,
	] );

	register_post_type( 'plates', [
		'labels'        => [
			'name'          => __( 'Тарелки' ), // основное название для типа записи
			'singular_name' => __( 'Plate' ), // название для одной записи этого типа
			'add_new'       => __( 'Добавить новый' ), // для добавления новой записи
			'menu_name'     => __( 'Тарелки' ), // название меню
		],
		'public'        => true,
		'menu_position' => 4,
		'menu_icon'     => 'dashicons-chart-pie',
		'supports'      => [ 'title', 'editor', 'thumbnail', 'post-formats' ],
		'taxonomies'    => [ 'category', 'tags' ],
		'has_archive'   => true,
	] );

	register_post_type( 'glasses', [
		'labels'        => [
			'name'          => __( 'Стаканы' ), // основное название для типа записи
			'singular_name' => __( 'Glass' ), // название для одной записи этого типа
			'add_new'       => __( 'Добавить новый' ), // для добавления новой записи
			'menu_name'     => __( 'Стаканы' ), // название меню
		],
		'public'        => true,
		'menu_position' => 4,
		'menu_icon'     => 'dashicons-coffee',
		'supports'      => [ 'title', 'editor', 'thumbnail', 'post-formats' ],
		'taxonomies'    => [ 'category', 'tags' ],
		'has_archive'   => true,
	] );

	register_post_type( 'boxes', [
		'labels'        => [
			'name'          => __( 'Контейнеры для еды' ), // основное название для типа записи
			'singular_name' => __( 'Boxes' ), // название для одной записи этого типа
			'add_new'       => __( 'Добавить новый' ), // для добавления новой записи
			'menu_name'     => __( 'Контейнеры для еды' ), // название меню
		],
		'public'        => true,
		'menu_position' => 4,
		'menu_icon'     => 'dashicons-archive',
		'supports'      => [ 'title', 'editor', 'thumbnail', 'post-formats', 'custom-fields' ],
		'taxonomies'    => [ 'category', 'tags' ],
		'has_archive'   => true,
	] );

	register_post_type( 'bags', [
		'labels'        => [
			'name'          => __( 'Упаковка для кондитерских изделий' ),
			'singular_name' => __( 'Bags' ),
			'add_new'       => __( 'Добавить новый' ),
			'menu_name'     => __( 'Упаковка для кондитерских изделий' ),
		],
		'public'        => true,
		'menu_position' => 4,
		'menu_icon'     => 'dashicons-portfolio',
		'supports'      => [ 'title', 'editor', 'thumbnail', 'post-formats' ],
		'taxonomies'    => [ 'category', 'tags' ],
		'has_archive'   => true,
	] );

	register_post_type( 'packages', [
		'labels'        => [
			'name'          => __( 'Бумажные пакеты' ), // основное название для типа записи
			'singular_name' => __( 'Packages' ), // название для одной записи этого типа
			'add_new'       => __( 'Добавить новый' ), // для добавления новой записи
			'menu_name'     => __( 'Бумажные пакеты' ), // название меню
		],
		'public'        => true,
		'menu_position' => 4,
		'menu_icon'     => 'dashicons-portfolio',
		'supports'      => [ 'title', 'editor', 'thumbnail', 'post-formats' ],
		'taxonomies'    => [ 'category', 'tags' ],
		'has_archive'   => true,
	] );

	register_post_type( 'sets', [
		'labels'        => [
			'name'          => __( 'Наборы посуды' ), // основное название для типа записи
			'singular_name' => __( 'Sets' ), // название для одной записи этого типа
			'add_new'       => __( 'Добавить новый' ), // для добавления новой записи
			'menu_name'     => __( 'Наборы посуды' ), // название меню
		],
		'public'        => true,
		'menu_position' => 4,
		'menu_icon'     => 'dashicons-grid-view',
		'supports'      => [ 'title', 'editor', 'thumbnail', 'post-formats' ],
		'taxonomies'    => [ 'category', 'tags' ],
		'has_archive'   => true,
	] );

    register_post_type( 'goods', [
        'labels'        => [
            'name'          => __( 'Хозяйственные товары' ), // основное название для типа записи
            'singular_name' => __( 'Goods' ), // название для одной записи этого типа
            'add_new'       => __( 'Добавить новый' ), // для добавления новой записи
            'menu_name'     => __( 'Хозяйственные товары' ), // название меню
        ],
        'public'        => true,
        'menu_position' => 4,
        'menu_icon'     => 'dashicons-admin-home',
        'supports'      => [ 'title', 'editor', 'thumbnail', 'post-formats' ],
        'taxonomies'    => [ 'category', 'tags' ],
        'has_archive'   => true,
    ] );

    register_post_type( 'chemistry', [
        'labels'        => [
            'name'          => __( 'Бытовая химия' ), // основное название для типа записи
            'singular_name' => __( 'Chemistry' ), // название для одной записи этого типа
            'add_new'       => __( 'Добавить новый' ), // для добавления новой записи
            'menu_name'     => __( 'Бытовая химия' ), // название меню
        ],
        'public'        => true,
        'menu_position' => 4,
        'menu_icon'     => 'dashicons-sos',
        'supports'      => [ 'title', 'editor', 'thumbnail', 'post-formats' ],
        'taxonomies'    => [ 'category', 'tags' ],
        'has_archive'   => true,
    ] );

    register_post_type( 'aluminum', [
        'labels'        => [
            'name'          => __( 'Посуда из алюминия' ), // основное название для типа записи
            'singular_name' => __( 'Aluminum' ), // название для одной записи этого типа
            'add_new'       => __( 'Добавить новый' ), // для добавления новой записи
            'menu_name'     => __( 'Посуда из алюминия' ), // название меню
        ],
        'public'        => true,
        'menu_position' => 4,
        'menu_icon'     => 'dashicons-sos',
        'supports'      => [ 'title', 'editor', 'thumbnail', 'post-formats' ],
        'taxonomies'    => [ 'category', 'tags' ],
        'has_archive'   => true,
    ] );

	register_post_type( 'different', [
		'labels'        => [
			'name'          => __( 'Дополнительные товары' ), // основное название для типа записи
			'singular_name' => __( 'Different' ), // название для одной записи этого типа
			'add_new'       => __( 'Добавить новый' ), // для добавления новой записи
			'menu_name'     => __( 'Дополнительные товары' ), // название меню
		],
		'public'        => true,
		'menu_position' => 4,
		'menu_icon'     => 'dashicons-share',
		'supports'      => [ 'title', 'editor', 'thumbnail', 'post-formats' ],
		'taxonomies'    => [ 'category', 'tags' ],
		'has_archive'   => true,
	] );
}


// Регистрируем таксономии

add_action( 'init', 'create_taxonomy_composition' ); //таксономия Состав

function create_taxonomy_composition() {
	register_taxonomy( 'composition', [ 'cutlery', 'plates', 'glasses', 'boxes', 'tubules', 'packages', 'different', 'sets', 'goods', 'chemistry', 'aluminum'], [
		'label'        => 'Состав', // определяется параметром $labels->name
		'rewrite'      => array( 'slug' => 'Состав' ),
		'hierarchical' => true
	] );
}

add_action( 'init', 'create_taxonomy_color' ); //таксономия Цвет

function create_taxonomy_color() {
	register_taxonomy( 'Цвет', [ 'cutlery', 'plates', 'glasses', 'boxes', 'tubules', 'packages', 'different', 'sets', 'goods', 'chemistry', 'aluminum' ], [
		'label'        => 'Цвет', // определяется параметром $labels->name
		'rewrite'      => array( 'slug' => 'Цвет' ),
		'hierarchical' => true
	] );
}

add_action( 'init', 'create_taxonomy_size' ); //таксономия объем

function create_taxonomy_size() {
	register_taxonomy( 'Объем', [ 'cutlery', 'plates', 'glasses', 'boxes', 'bags', 'packages', 'different', 'sets', 'goods', 'chemistry', 'aluminum' ], [
		'label'        => 'Объем', // определяется параметром $labels->name
		'rewrite'      => array( 'slug' => 'Объем' ),
		'hierarchical' => true
	] );
}

add_action( 'init', 'create_taxonomy_features' ); //таксономия Особенности

function create_taxonomy_features() {
	register_taxonomy( 'Особенности', [ 'cutlery', 'plates', 'glasses', 'boxes', 'bags', 'packages', 'different', 'sets', 'goods', 'chemistry', 'aluminum' ], [
		'label'   => __('Особенности'),
		'rewrite' => [ 'slug' => 'features' ],
	] );
}


// Adding custom templates for pages

add_filter( 'template_include', 'my_template' );

function my_template( $template ) {
	if ( is_post_type_archive( [ 'cutlery', 'plates', 'glasses', 'boxes', 'bags', 'packages', 'different', 'sets', 'goods', 'chemistry', 'aluminum' ] )) {

		add_action( 'wp_enqueue_scripts', 'styleCategoryPage' );

		function styleCategoryPage() {
			wp_enqueue_style( 'category', get_template_directory_uri() . '/styles/pages/category.css' );
		}

		add_action( 'wp_footer', 'jsCategory' );

		function jsCategory() {

			wp_enqueue_script( 'category', get_template_directory_uri() . '/js/pages/category.js' );
		}

		return get_stylesheet_directory() . '/page-templates/category.php';
	}

	if ( is_front_page() ) {
		add_action( 'wp_enqueue_scripts', 'styleHome' );

		function styleHome() {
			wp_enqueue_style( 'jquery-ui-slider' );
			wp_enqueue_style( 'home', get_template_directory_uri() . '/styles/pages/home.css' );
		}

		add_action( 'wp_footer', 'jsHome' );

		function jsHome() {
			wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/lib/slick.min.js', [ 'jquery' ] );
			wp_enqueue_script( 'home', get_template_directory_uri() . '/js/pages/home.js', [ 'jquery', 'slick' ] );
		}

		return get_stylesheet_directory() . '/page-templates/home.php';
	}

	if ( is_page('contacts') ) {
		add_action( 'wp_enqueue_scripts', 'styleContacts' );

		function styleContacts() {
			wp_enqueue_style( 'contacts', get_template_directory_uri() . '/styles/pages/contacts.css' );
		}

		return get_stylesheet_directory() . '/page-templates/contacts.php';
	}

	if ( is_page('blog') ) {
		add_action( 'wp_enqueue_scripts', 'styleBlog' );

		function styleBlog() {
			wp_enqueue_style( 'blog', get_template_directory_uri() . '/styles/pages/blog.css' );
		}

		return get_stylesheet_directory() . '/page-templates/blog.php';
	}

	if ( is_page('about-us') ) {
		add_action( 'wp_enqueue_scripts', 'styleAboutUs' );

		function styleAboutUs() {
			wp_enqueue_style( 'about-us', get_template_directory_uri() . '/styles/pages/about-us.css' );
		}

		return get_stylesheet_directory() . '/page-templates/about-us.php';
	}

	if ( is_page('partnership') ) {
		add_action( 'wp_enqueue_scripts', 'stylePartnership' );

		function stylePartnership() {
			wp_enqueue_style( 'partnership', get_template_directory_uri() . '/styles/pages/partnership.css' );
		}

		return get_stylesheet_directory() . '/page-templates/partnership.php';
	}

	return $template;

}

// Adding JS

add_action( 'wp_footer', 'jsMobileMenu' );

function jsMobileMenu() {
	wp_enqueue_script( 'mobile_menu', get_template_directory_uri() . '/js/components/mobile_menu.js' );
}

add_action( 'wp_footer', 'jsUpButton' );

function jsUpButton() {
	wp_enqueue_script( 'up_button', get_template_directory_uri() . '/js/components/up-button.js' );
}

add_action( 'wp_footer', 'jsContactForm' );

function jsContactForm() {
	wp_enqueue_script( 'contact_form', get_template_directory_uri() . '/js/components/form.js' );
}

add_action( 'wp_footer', 'jsPopUp' );

function jsPopUp() {
	wp_enqueue_script( 'popup', get_template_directory_uri() . '/js/components/pop-up.js', ['jquery'] );
}

function jsTabs() {
	wp_enqueue_script( 'tabs', get_template_directory_uri() . '/js/components/tabs.js' );
}

add_action( 'wp_footer', 'jsTabs' );

// Adding custom templates for pages

add_theme_support( 'custom-logo' );
add_theme_support( 'post-thumbnails' );

// Adding Global Variables
function js_variables(){
	$variables = [
		'urls' => [
			'contact_form' => get_template_directory_uri() . '/Controllers/contact_form.php',
		],
		'is_mobile' => wp_is_mobile()
	];
	echo(
		'<script type="text/javascript">window.wp_data = ' . json_encode($variables) . '</script>'
    );
}

add_action('wp_head','js_variables');


// Adding ajax controllers
add_action('wp_ajax_get_posts'       , '/Controllers/contact_form');
add_action('wp_ajax_nopriv_get_posts', '/Controllers/contact_form');
?>