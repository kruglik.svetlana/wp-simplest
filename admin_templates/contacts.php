<section class="contacts-section">
    <h1 class="title">Контакты</h1>
    <p class="text">Экопралеска - ваш источник в выборе биоразлагаемой посуды!!</p>
    <ul class="list">
        <li class="item">
            <div class="icon -geolocation"></div>
            <p class="text">Юридический адрес: г.Минск, ул.Космонавтов, 8-20</p>
        </li>
        <li class="item">
            <div class="icon -mail"></div>
            <p class="text">Почтовый адрес: 220025, г.Минск, ул.Космонавтов, 8-20</p>
        </li>
        <li class="item">
            <div class="icon -phone"></div>
            <p class="text">
                Отдел продаж: +375 (29) 116-05-71
                Бухгалтерия: +375 (29) 224-00-50
            </p>
        </li>
    </ul>
</section>

