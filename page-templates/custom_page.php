<?php get_template_part( 'templates/head' ); ?>
<?php get_template_part( 'templates/header/wrapper' ); ?>
<?php get_template_part( 'templates/components/banner' ); ?>

<?php
$currentCategory = get_queried_object();

$args = array(
	'post_type' => $currentCategory->name
);

$the_query = new WP_Query( $args );
$main_fields = get_fields($the_query->posts[0]);
?>


<?php get_template_part( 'templates/form' ); ?>
<?php get_template_part( 'templates/footer/wrapper' ); ?>



