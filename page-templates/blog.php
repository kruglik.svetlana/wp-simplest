<?php
$currentPage = get_queried_object();
?>

<?php get_template_part( 'templates/head' ); ?>
<?php get_template_part( 'templates/header/wrapper' ); ?>
<?php get_template_part( 'templates/components/banner', null, ['banner_url' => get_field('banner')] ); ?>

<section class="blog-section">
    <h1 class="title"><?= $currentPage->post_title ?></h1>
    <p class="text"><?= get_field('description') ?></p>
    <article class="grid-post-container">
	    <?= $currentPage->post_content ?>
    </article>
</section>

<?php get_template_part( 'templates/form' ); ?>
<?php get_template_part( 'templates/footer/wrapper' ); ?>



