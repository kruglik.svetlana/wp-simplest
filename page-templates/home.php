<?php get_template_part( 'templates/head' ); ?>
<?php get_template_part( 'templates/header/wrapper' ); ?>
<?php get_template_part( 'templates/components/slider' ); ?>
<?php get_template_part( 'templates/components/catalog' ); ?>
<?php get_template_part( 'templates/components/advantages' ); ?>
<?php get_template_part( 'templates/components/dignity' ); ?>
<?php get_template_part( 'templates/form' ); ?>
<?php get_template_part( 'templates/footer/wrapper' ); ?>



