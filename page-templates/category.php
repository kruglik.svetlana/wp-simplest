<?php
$currentCategory = get_queried_object();
$mainPost = null;

$args = array(
	'post_type' => $currentCategory->name
);

$the_query = new WP_Query( $args );

foreach(array_keys($the_query->posts) as $key){
	if ($the_query->posts[$key]->post_name === 'main'){
		$mainPost = $the_query->posts[$key];
	}
};

$main_fields = get_fields($mainPost);
$banner_url = $main_fields['banner'];
?>

<?php get_template_part( 'templates/head' ); ?>
<?php get_template_part( 'templates/header/wrapper' ); ?>
<?php get_template_part( 'templates/components/banner', null, ['banner_url' => $banner_url] ); ?>

<div class="banner-section">
    <h2 class="title"><?= __( $currentCategory->label ); ?></h2>
    <p class="description">
	    <?= __( $main_fields['category_description'] ); ?>
    </p>
</div>

<?php get_template_part( 'templates/components/products-grid' ); ?>
<?php get_template_part( 'templates/form' ); ?>
<?php get_template_part( 'templates/footer/wrapper' ); ?>
