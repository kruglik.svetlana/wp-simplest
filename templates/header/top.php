<section class="top-section">
	<?php get_template_part( 'templates/components/logo' ); ?>
	<?php get_template_part( 'templates/components/social_list' ); ?>
    <button id="menu_toggle_open" class="button -burger">
        <span class="line"></span>
    </button>
    <div class="button-block">
        <div class="item">
            <a class="telephone" href="tel:+375291160571">+375 (29) 116-05-71</a>
            <a class="button -icon -phone" href="tel:+375291160571"></a>
        </div>
        <div class="item">
            <a class="price-list" href="https://drive.google.com/drive/folders/18DsSOPHkE88bfFKzA9dh6cismKV4heoz?usp=sharing">Посмотреть прайс</a>
            <a class="button -icon -list" href="https://drive.google.com/drive/folders/18DsSOPHkE88bfFKzA9dh6cismKV4heoz?usp=sharing"></a>
        </div>
    </div>
</section>
