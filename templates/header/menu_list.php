<?php
$postId        = $post->ID;
$menuLocations = get_nav_menu_locations();
$menuID        = $menuLocations['primary'];
$navItems      = wp_get_nav_menu_items( $menuID );
$categoryItems = get_taxonomy( 'category' );
?>

<section class="menu-list-section">
    <nav id="header_menu" class="header-menu">
        <button id="menu_toggle_close" class="button -icon -close"></button>

        <ul id="menu-list" class="menu-list">
            <li class="item -parent">
                <a href="#"><?= __( 'Каталог' ) ?></a>
                <ul class="submenu">
					<?php foreach ( $categoryItems->object_type as $categoryItem ) {
						$post = get_post_type_object( $categoryItem );

						if ( $post->name === 'post' ) {
							continue;
						}

						echo '<li class="item"><a href="/' . $post->name . '">' . $post->label . '</a></li>';
					} ?>
                </ul>
            </li>

	        <?php foreach ( $navItems as $navItem ) {
		        echo '<li class="item"><a class="' . ( $navItem->object_id == $postId ? ' -active' : '' ) . '" href="' . $navItem->url . '" title="' . $navItem->title . '">' . $navItem->title . '</a></li>';
	        } ?>
        </ul>
    </nav>
</section>
