<?php
$menuLocations = get_nav_menu_locations();
$menuID        = $menuLocations['primary'];
$navItems      = wp_get_nav_menu_items( $menuID );
$categoryItems = get_taxonomy( 'category' );
?>

<footer class="footer-section">
    <div class="container">
        <div class="footer-block item-1">
            <div class="contact-block">
                <a href="mailto:sales@ecopraleska.com">sales@ecopraleska.com</a>
                <a href="tel:+375291160571">+375 (29) 116-05-71</a>
                <a href="tel:+375292240050">+375 (29) 224-00-50</a>
                <a href="tel:+375339166775">+375 (33) 916-67-75 </a>
            </div>
        </div>
        <div class="footer-block tabs-section item-2">
            <a class="menu-name tab-button"><?=__('Главное меню') ?></a>
            <ul class="menu-list tab-content">
				<?php foreach ( $navItems as $navItem ) {
					echo '<li class="item"><a href="' . $navItem->url . '" title="' . $navItem->title . '">' . $navItem->title . '</a></li>';
				} ?>
            </ul>
            <a class="menu-name tab-button"><?=__('Каталог товаров') ?></a>
            <ul class="menu-list tab-content">
				<?php foreach ( $categoryItems->object_type as $categoryItem ) {
					$post = get_post_type_object( $categoryItem );
					if ( $post->name === 'post' ) {
						continue;
					}

					echo '<li class="item"><a href="' . '/' . $post->name . '">' . $post->label . '</a></li>';
				} ?>
            </ul>
        </div>
        <div class="footer-block item-3">
            <span class="line"><?=__('© 2021, ЭкоПралеска') ?></span>
            <div class="social-block">
				<?php get_template_part( 'templates/components/social_list' ); ?>
            </div>
        </div>
    </div>
	<?php get_template_part( 'templates/footer/up-button' ); ?>
</footer>
<?php wp_footer(); ?>
</body>
</html>