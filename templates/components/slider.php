<section class="slider-section">
    <ul id="home-slider" class="slick-slider-section">
        <li class="slide">
            <img src="http://ecopraleska.com/wp-content/themes/simplest/styles/images/slide1.jpg"
                 alt="Photo"
                 class="slide-img">
        </li>
        <li class="slide">
            <img src="http://ecopraleska.com/wp-content/themes/simplest/styles/images/slide11.jpg"
                 alt="Photo"
                 class="slide-img">
        </li>
        <li class="slide">
            <img src="http://ecopraleska.com/wp-content/themes/simplest/styles/images/banner/packages-brad.jpg"
                 alt="Photo"
                 class="slide-img">
        </li>
    </ul>
    <div class="text-block">
        <span class="line"><?= __('ЭкоПралеска - ваш источник в выборе биоразлагаемой посуды.') ?></span>
        <span class="line"><?= __('Экологичная одноразовая посуда по оптовым ценам.') ?></span>
    </div>
</section>
