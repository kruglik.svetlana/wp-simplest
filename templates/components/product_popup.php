<?php
$product = $args['product'];
?>

<div class="product-popup popup-container -<?= $product->ID ?>">
    <button class="popup-close button -icon -close"></button>
    <div class="container">
        <div class="grid-block item-1">
            <img class="image-card-overlay"
                 src="<?= get_field('image') ?>"
                 alt="<?= $product->post_title ?>"/>
            <span class="line"></span>
        </div>
        <div class="grid-block item-2">
            <h3 class="title"><?= $product->post_title ?></h3>
	        <?= get_field('description') ?>
        </div>
    </div>
</div>