<section class="advantages-section">
    <h1 class="title"><?= __('Преимущества экопосуды') ?></h1>
    <div class="advantages-block">
        <ul class="advantages-list">
            <li class="item">
                <div class="advantages-circle"
                     style="background-image: url('http://ecopraleska.com/wp-content/themes/simplest/styles/images/eco-friendly.png')"
                     alt="">
                </div>
                <span class="title"><?= __('Экологична') ?></span>
                <p class="text">
                    <?= __('Экопосуда легко утилизируется, не наносит вред окружающей среде, в отличие от одноразовой
                    посуды из пластика, которая разлагается более 500 лет.') ?>
                </p>
            </li>
            <li class="item">
                <div class="advantages-circle"
                     style="background-image: url('http://ecopraleska.com/wp-content/themes/simplest/styles/images/chemical-free.png')"
                     alt="">
                </div>
                <span class="title"><?= __('Безвредна') ?></span>
                <p class="text">
	                <?= __('Безопасна для здоровья. Одноразовая посуда из сахарного тростика, дерева, крахмала, бумаги
                    сохраняет вкус еды и напитков, не выделяя вредных веществ при нагревании. За счет особых свойств
                    природного материала она выдерживает достаточно сильные перепады температур.') ?>
                </p>
            </li>
            <li class="item">
                <div class="advantages-circle"
                     style="background-image: url('http://ecopraleska.com/wp-content/themes/simplest/styles/images/organic.png')"
                     alt="">
                </div>
                <span class="title"><?= __('Эстетична') ?></span>
                <p class="text">
                    <?= __('Экопосуда является легкой, прочной, элегантной на вид, приятной на ощупь и не имеет запаха.
                    Экология - это стиль сегодняшней жизни. Это модно, это красиво, это эстетично!') ?>
                </p>
            </li>
        </ul>
    </div>
    <div class="grid-block">
        <div class="block item-1"
           style="background-image: url('http://ecopraleska.com/wp-content/themes/simplest/styles/images/eco1.jpg')">
            <h2 class="title"></h2>
        </div>
        <div class="block item-2">
            <h2 class="title"><?= __('Берегите себя и Планету!') ?></h2>
            <p class="text">
	            <?= __('Выбор экопосуды – это Ваш вклад в экологию планеты уже сегодня, свидетельство современного мышления,
                сознательного движения к защите окружающей среды и сохранению здоровья людей, начиная даже с таких
                простых бытовых вопросов. А ее применение на официальных мероприятиях повышает престиж и авторитет
                организаторов.') ?>
            </p>
        </div>
        <div class="block item-3">
            <h2 class="title"><?= __('Выбирайте безопасность!') ?></h2>
            <p class="text">
                <?= __('Экопосуда безопасна для здоровья людей. В отличие, например, от пластика, который при контакте с
                алкогольными напитками и нагревании может выделять токсичные соединения и канцерогены. Кроме того,
                полимерные материалы, из которых полностью состоят пластиковые изделия, подвержены процессу старения.
                В результате из них выделяются вредные вещества, которые переходят в пищевые продукты, соприкасающиеся
                с такой тарой.') ?>
            </p>
        </div>
        <div class="block item-4"
           style="background-image: url('http://ecopraleska.com/wp-content/themes/simplest/styles/images/eco2.jpg')">
            <h2 class="title"></h2>
        </div>
    </div>
</section>