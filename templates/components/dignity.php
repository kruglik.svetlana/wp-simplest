<section class="dignity-section">
    <h2 class="title"><?=__('Что Вы получаете, работая с нами?') ?></h2>
    <ul class="list">
        <li class="item">
            <span class="icon -delivery"></span>
            <span class="line"><?=__('Профессионализм!') ?></span>
            <p class="text"><?=__('Всегда приятно иметь дело с профессионалами.') ?></p>
        </li>
        <li class="item">
            <span class="icon -cool"></span>
            <span class="line"><?=__('Гарантию качества!') ?></span>
            <p class="text"><?=__('Мы предлагаем только качественную экопосуду от ведущих производителей.') ?></p>
        </li>
        <li class="item">
            <span class="icon -phone"></span>
            <span class="line"><?=__('Экономию времени и средств!') ?></span>
            <p class="text"><?=__('Заказать товар у нас в один клик или по звонку. Наш сервис экономит Ваше время, а цены - Ваши деньги!') ?></p>
        </li>
        <li class="item">
            <span class="icon -delivery"></span>
            <span class="line"><?=__('Склад в центре города!!') ?></span>
            <p class="text"><?=__('Забрать товар можно по адресу: г.Минск, пер.Северный, 13/14') ?></p>
        </li>
    </ul>
</section>