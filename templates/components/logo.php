<div class="logo-block">
    <a class="logo" href="<?php echo home_url(); ?>">
		<?php
		$logo_img = '';
		if ( $custom_logo_id = get_theme_mod( 'custom_logo' ) ) {
			$logo_img = wp_get_attachment_image( $custom_logo_id, 'full', false, array(
				'class' => 'logo',
			) );
		}

		echo $logo_img;
		?>

        <span class="text"><?php bloginfo( 'name' ); ?></span>
    </a>
</div>
