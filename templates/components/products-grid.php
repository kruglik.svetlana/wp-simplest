<?php
$currentCategory = get_queried_object();
$args = array(
	'post_type' => $currentCategory->name
);

$the_query = new WP_Query($args);
?>

<section class="grid-products-block">
	<?php
	if ( $the_query->have_posts() ) :
		while ( $the_query->have_posts() ) :
			$the_query->the_post();
			if ( $post->post_title === 'main' ) continue;
	    echo '<div class="card products-1" data-product-id="' . $post->ID . '">' .
                '<img class="image-card" src="' . get_field('image') . '" alt="Photo cutlery"/>
                <span class="title-card">' . $post->post_title . '</span>
              </div>';
			get_template_part( 'templates/components/product_popup', null, ['product' => $post] );
		endwhile;
		wp_reset_postdata();
	endif;
	?>
</section>