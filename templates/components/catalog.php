<section class="catalog-grid-section">
    <h1 class="title"><?= __('Каталог товаров') ?></h1>
    <div class="grid-block">
        <a class="block item-1" href="https://ecopraleska.com/plates/" style="background-image: url('http://ecopraleska.com/wp-content/themes/simplest/styles/images/plates10.jpg')">
            <h2 class="title"><?=__('Тарелки, супницы') ?></h2>
        </a>
        <a class="block item-2" href="https://ecopraleska.com/cutlery/" style="background-image: url('http://ecopraleska.com/wp-content/themes/simplest/styles/images/cutlery1.jpg')">
            <h2 class="title"><?= __('Приборы') ?></h2>
        </a>
        <a class="block item-3" href="https://ecopraleska.com/glasses/"  style="background-image: url('http://ecopraleska.com/wp-content/themes/simplest/styles/images/glasses100.jpg')">
            <h2 class="title"><?= __('Одноразовые стаканы') ?></h2>
        </a>
        <a class="block item-4" href="https://ecopraleska.com/boxes/"  style="background-image: url('http://ecopraleska.com/wp-content/themes/simplest/styles/images/boxes10.jpg')">
            <h2 class="title"><?= __('Контейнеры для еды') ?></h2>
        </a>
        <a class="block item-5" href="https://ecopraleska.com/different/"  style="background-image: url('http://ecopraleska.com/wp-content/themes/simplest/styles/images/tubules10.jpg')">
            <h2 class="title"><?= __('Дополнительные товары') ?></h2>
        </a>
        <a class="block item-6" href="https://ecopraleska.com/packages/"  style="background-image: url('http://ecopraleska.com/wp-content/themes/simplest/styles/images/packages10.jpg')">
            <h2 class="title"><?= __('Бумажные пакеты') ?></h2>
        </a>
        <a class="block item-7" href="https://ecopraleska.com/bags/"  style="background-image: url('http://ecopraleska.com/wp-content/themes/simplest/styles/images/different10.jpg')">
            <h2 class="title"><?= __('Упаковка для кондитерских изделий') ?></h2>
        </a>
    </div>
</section>
