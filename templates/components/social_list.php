<section class="social-list-section">
    <ul class="list">
        <li class="item">
            <a class="icon -insta" href="https://instagram.com/eco.praleska?igshid=1m6ozhf37in78"></a>
        </li>
        <li class="item">
            <a class="icon -telegram" href="https://telegram.me/tatyanaprushak"></a>
        </li>
        <li class="item">
            <a class="icon -viber" href="viber://add?number=375292240050"></a>
        </li>
    </ul>
</section>
