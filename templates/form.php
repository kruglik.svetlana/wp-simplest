<section class="form-section" id="form">
    <div class="container">
        <h2 class="title"><?=__('Заполните форму и мы свяжемся с Вами для обсуждения деталей заказа!') ?></h2>
        <form class="form" method="POST" id="contact_form">
            <label class="label" for="input_name"><?=__('Как Вас зовут?*') ?></label>
            <input class="input" name="name" type="text" required placeholder="Имя" id="input_name">
            <label class="label" for="input_tel"><?=__('Ваш телефон*') ?></label>
            <input class="input" name="phone" type="tel" pattern="\+375[0-9]{2}[0-9]{3}[0-9]{2}[0-9]{2}"
                   placeholder="+375*********" required value="+375" id="input_tel">
            <label class="label" for="input_tel"><?=__('Ваш email*') ?></label>
            <input class="input" name="email" type="email" required placeholder="Email*" id="input_email">
            <textarea class="textarea" name="message" placeholder="Ваш комментарий" rows="5" maxlength="40"></textarea>
            <button class="button -rectangle" type="button" id="subscribe_button"><?=__('Отправить') ?></button>
            <span class="line">
                <?=__('Отправка формы подтверждает Ваше согласие на обработку персональных данных.') ?>
            </span>
        </form>
    </div>
</section>

<section class="pop-up-section popup-container -contact-success">
    <div class="pop-up-block">
        <button class="popup-close button -close"></button>
        <h3 class="title"><?=__('Спасибо за заявку!') ?></h3>
        <p class="text"><?=__('Мы свяжемся с Вами в ближайшее время!') ?></p>
    </div>
</section>